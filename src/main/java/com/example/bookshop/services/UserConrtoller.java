package com.example.bookshop.services;

import com.example.bookshop.entity.User;
import com.example.bookshop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azabavskaya on 1/24/17.
 */
@Controller
@RequestMapping("/users")
public class UserConrtoller {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/all")
    List<User> getAllUsers (){
        return new ArrayList<>();
    }

    @RequestMapping(value = "/{id}")
    User getUserById(){
        return new User();
    }

    @RequestMapping(value = "/create")
    User createUser(){
        return new User();
    }

    @RequestMapping(value = "/update")
    void updateUser(){

    }
}
