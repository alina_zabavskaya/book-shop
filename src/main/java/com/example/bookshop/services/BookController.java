package com.example.bookshop.services;

import com.example.bookshop.entity.Book;
import com.example.bookshop.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azabavskaya on 1/24/17.
 */
@Controller
@RequestMapping("/books")
public class BookController {

    @Autowired
    BookRepository bookRepository;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    List<Book> getAllBook(){
        return new ArrayList<Book>();
    }

    @RequestMapping(value = "/create", method = RequestMethod.PUT)
    Book createBook (String title, String author, Double price){
        return new Book();
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    void deleteBook(String id){}

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    void updateBook(){
    }
}
