package com.example.bookshop.repositories;

import com.example.bookshop.entity.Book;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by azabavskaya on 1/24/17.
 */
public interface BookRepository extends CrudRepository<Book,String> {

}
