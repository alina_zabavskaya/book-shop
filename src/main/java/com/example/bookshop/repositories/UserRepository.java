package com.example.bookshop.repositories;

import com.example.bookshop.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by azabavskaya on 1/24/17.
 */
public interface UserRepository extends CrudRepository <User,String> {

}
